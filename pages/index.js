import { useState, useEffect } from 'react'

import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Instagram from 'instagram-web-api'
import moment from 'moment'

export default function Home({ posts }) {
  const [currentWeather, setCurrentWeather] = useState()

  useEffect(() => {
    const url =
      'https://api.met.no/weatherapi/locationforecast/2.0/compact.json?altitude=0&lat=-34.10896875576211&lon=18.46971602476739'
    fetch(url)
      .then((res) => res.json())
      .then((data) => setCurrentWeather(data))
  }, [])

  const videoPosts = posts.filter((el) => el.node.is_video)

  return (
    <div className={styles.container}>
      <Head>
        <title>RIDETHEBERG</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <h1 className='my-auto mt-8 text-3xl font-bold'>Ride The Berg</h1>

      {currentWeather ? (
        <p className='m-2 text-sm text-gray-500'>
          Current weather in Muizenberg:{' '}
          {
            currentWeather.properties.timeseries[0].data.instant.details
              .air_temperature
          }
          °C /{' '}
          {
            currentWeather.properties.timeseries[0].data.instant.details
              .wind_speed
          }
          km/h
        </p>
      ) : (
        <p className='m-2 text-sm text-gray-500'>☀ Loading weather data...</p>
      )}

      {videoPosts && (
        <ul className='divide-y'>
          {videoPosts.map(({ node }, i) => {
            return (
              <li key={i} className='py-12 text-center'>
                {/* <img src={node.display_resources[0].src} /> */}
                <p className='mb-3 text-gray-500'>
                  Published{' '}
                  <span className='px-3 py-2 ml-2 text-gray-900 bg-gray-200 rounded-full'>
                    {moment(node.taken_at_timestamp * 1000).fromNow()}
                  </span>
                  {/* <span className="px-4 text-gray-500">{ moment(node.taken_at_timestamp * 1000).format('D MMMM Y, h:mm a') }</span> */}
                  <span className='px-4 text-gray-500'>
                    {moment(node.taken_at_timestamp * 1000).format('h:mm a')}
                  </span>
                </p>
                <video
                  controls
                  className='mx-auto rounded-lg outline-none cursor-pointer focus:outline-none'
                  poster={node.thumbnail_src || node.thumbnail_resources[0].src}
                >
                  <source type='video/mp4' src={node.video_url} />
                </video>
                <p className='inline-block px-3 py-2 my-4 text-gray-900 bg-gray-200 rounded-lg'>
                  {node.edge_media_to_caption.edges[0]?.node.text}
                </p>
                <p className='text-gray-500'>
                  {node.video_view_count} views{' '}
                  <span className='px-3 py-2 ml-2 bg-green-400 rounded-full text-gray-50'>
                    ♡ {node.edge_media_preview_like.count}
                  </span>
                </p>
              </li>
            )
          })}
        </ul>
      )}
      <p className='px-4 py-3 mb-8 text-sm bg-gray-100 rounded-lg'>
        Weather data provided by the{' '}
        <a href='https://developer.yr.no/doc/GettingStarted/'>yr.no API</a>
      </p>
    </div>
  )
}

export async function getStaticProps(context) {
  const client = new Instagram({
    username: process.env.INSTAGRAM_USERNAME,
    password: process.env.INSTAGRAM_PASSWORD,
  })
  await client.login()

  const response = await client.getPhotosByUsername({
    username: 'lifestylesurfshop',
    first: 5,
  })

  return {
    props: {
      posts: response.user.edge_owner_to_timeline_media.edges,
    },
    revalidate: 60,
  }
}
