function handler(req, res) {
  if (req.method === 'POST') {
    const userName = req.body.userName

    if (!userName || userName.includes('@')) {
      res.status(422).json({ message: 'Invalid username received.' })
      return
    }

    const response = await fetch(`https://www.instagram.com/${userName}`);
    const data = await response.text();
    res.status(200).json({ posts: data })

    return
  } else {
    res.status(200).json({ message: 'GET request received' })
  }
}

export default handler
